import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClientsComponent } from './clients.component';
import { NgbModule, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModule } from '../../components/components.module';
import { FromService } from '../../providers/form.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        ComponentsModule
    ],
    declarations: [
        ClientsComponent
    ],
    entryComponents: [
    ],
    exports: [
        ClientsComponent
    ],
    providers: [
        NgbCarouselConfig,
        FromService
    ]
})

export class ClientsModule { }
