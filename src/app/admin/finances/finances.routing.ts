import { Routes } from '@angular/router';
import { BadDebtsComponent } from './bad-debts/bad-debts.component';
import { ClientsComponent } from './clients/clients.component';
import { InvoicesComponent } from './invoices/invoices.component';

export const FinancesRoutes: Routes = [
    {
        path: 'clientes',
        component: ClientsComponent
    },
    {
        path: 'malas-deudas', 
        component: BadDebtsComponent
    },
    {
        path: 'facturas',
        component: InvoicesComponent
    }
];
