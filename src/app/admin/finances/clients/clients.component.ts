import { Component, OnInit, OnDestroy } from '@angular/core';
import { ClientsService } from '../../../services/clients.service';
import { Subscription, empty } from 'rxjs';
import { BroadcastService } from '../../../services/broadcast.service';
import { SharedComponent } from '../../../model/shared-component';
import { DebtsComponent } from './debts/debts.component';
import { ModalComponent } from '../../../components/modal/modal.component';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit, OnDestroy {
  dataTableConfig = {
    config: {
      base: this.clients,
      api: 'getClients',
      order: [[3, 'desc']]
    },
    columns: [
      {
        display: 'Nombre',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Suscripción',
        field: 'subscription_status',
        type: 'label',
        orderable: false,
        options: {
          emptyText: 'Faltante'
        }
      },
      {
        display: 'Tarjeta por expirar',
        field: 'cards',
        type: 'card-expire',
        orderable: false
      },
      {
        display: 'Balance MXN',
        field: 'debt',
        type: 'number'
      },
      {
        display: '',
        field: '',
        type: 'inline-button',
        options: [
          {
            cssClass: 'btn btn-primary',
            name: 'Deudas',
            event: 'client.debts',
            conditionality: 'true'
          }
        ]
      }
    ]
  };

  broadcast$: Subscription;

  constructor(
    private readonly clients: ClientsService,
    private readonly broadcast: BroadcastService,
    public appModal: ModalComponent
  ) { }

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'client.debts': this.debtsItem(event.data); break;
      }
    });
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  debtsItem(data): void {
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(DebtsComponent, data, { title: `${data.name}` });
    this.appModal.open(props);
  }
}
