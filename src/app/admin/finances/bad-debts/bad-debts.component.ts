import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { SwalService } from '../../../services/swal.service';
import { BroadcastService } from '../../../services/broadcast.service';
import { BadDebtsService } from '../../../services/bad-debts.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-bad-debts',
  templateUrl: './bad-debts.component.html',
  styleUrls: ['./bad-debts.component.scss']
})
export class BadDebtsComponent implements OnDestroy, OnInit {
  dataTableConfig = {
    config: {
      base: this.badDebts,
      api: 'getBadDebtsClients'
    },
    columns: [
      {
        display: 'Nombre',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Grupo de ventas',
        field: 'group.name',
        type: 'text'
      },
      {
        display: 'Fecha de alta',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'Email',
        field: 'email',
        type: 'text'
      },
      {
        display: 'Deudas',
        field: 'debts',
        type: 'date'
      },
      {
        display: 'ACCIONES',
        field: '',
        type: 'actions',
        options: [
          {
            display: 'Restaurar al cliente',
            event: 'client.restore',
            conditionality: 'true'
          }
        ]
      }
    ]
  };

  broadcast$: Subscription;
  constructor(
    private readonly badDebts: BadDebtsService,
    private readonly broadcast: BroadcastService,
    public swal: SwalService
  ) { }

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'client.restore': this.restoreClientItem(event); break;
      }
    });
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  restoreClientItem(data): void  {
    this.swal.warning({ title: '¿Desea restaurar al cliente?' }).then(result => {
      if (result.value) {
        this.badDebts.restoreClient(data.data.id_clients).subscribe((response: any) => {
          if (response.success) {
            this.swal.success();
          } else {
            this.swal.error({ title: response.message });
          }
        });
      }
    });
  }
}
