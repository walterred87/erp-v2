import { Component, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { TicketsService } from '../../../services/tickets.service';
import { SharedComponent } from '../../../model/shared-component';
import { AssignComponent } from '../tickets/assign/assign.component';
import { ModalComponent } from '../../../components/modal/modal.component';
import { ReassingComponent } from '../tickets/reassing/reassing.component';
import { SwalService } from '../../../services/swal.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BroadcastService } from '../../../services/broadcast.service';
import { AgmMap } from '@agm/core';

@Component({
  selector: 'app-tickets-map',
  templateUrl: './tickets-map.component.html',
  styleUrls: ['./tickets-map.component.scss']
})
export class TicketsMapComponent implements OnInit {
  // @ViewChild(AgmMap, {static: false}) agmMap: AgmMap
  tickets: any;
  lat = 20.673135;
  lng = -103.350693;
  zoom = 4;
  previous;

  constructor(
    private readonly broadcast: BroadcastService,
    private readonly ticketService: TicketsService, 
    private readonly appModal: ModalComponent,
    private readonly swal: SwalService,
    public activeModal: NgbActiveModal
    ) { }

  ngOnInit(): void {
    this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'reload-map':
          this.ticketService.getMapTickets().subscribe((resp: any) => {
            this.tickets = resp.data.tickets;
          });
          // this.agmMap.triggerResize(true);
          // triggerResize()
          break;
      }
    });
    this.ticketService.getMapTickets().subscribe((resp: any) => {
      this.tickets = resp.data.tickets;
      switch (resp.data.team) {
        case 1:
          this.lat = 20.673135;
          this.lng = -103.350693;
          this.zoom = 9;
          break;
        case 2:
          this.lat = 20.967064;
          this.lng = -89.623728;
          this.zoom = 12;
          break;
        default:
          this.lat = 20.673135;
          this.lng = -103.350693;
          this.zoom = 6;
          break;
      }
    });
  }

  clickedMarker(infowindow): void {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
  }

  ticketStatusUrl(status, type): any {
    if (type === 'removal' || type === 'move') {
      // tslint:disable-next-line: no-parameter-reassignment
      type = 'service_call';
    }

    return `assets/img/tickets_status/${status}-marker-${type}.png`;
  }

  assign(data): void {
    Object.assign(data, {isMap: true});
    const props: SharedComponent = new SharedComponent(AssignComponent, data, {title: 'Asignación de ticket'});
    this.appModal.openXl(props);
  }

  reassign(data): void {
    Object.assign(data, {isMap: true});
    const props: SharedComponent = new SharedComponent(ReassingComponent, data, {title: 'Reasignación de ticket'});
    this.appModal.openXl(props);
  }

  unassign(data): void {
    const swalParams = {
      title: '¿Desea desasignar este ticket?',
      text: '¡Si desasigna este ticket el estatus pasara a abierto!'
    };

    this.swal.warning(swalParams).then(result => {        
      if (result.value) {
        const params = { 
          status: 'unassigned'
        };
        this.ticketService.getStatus(params, data.id_tickets).subscribe((respData: any) => {
          if (respData.success) {
            this.swal.success().then(() => {
              this.ticketService.getMapTickets().subscribe((resp: any) => {
                this.tickets = resp.data.tickets;
              });
            });
          } else {
            this.swal.error({title: 'Ocurrió un error al actualizar los datos'});
          }
      });
      }
    });
}

}
