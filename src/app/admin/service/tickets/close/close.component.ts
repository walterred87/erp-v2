import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { SwalService } from '../../../../services/swal.service';
import { BroadcastService } from '../../../../services/broadcast.service';
import { TicketsService } from '../../../../services/tickets.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { FromService } from '../../../../providers/form.service';

@Component({
  selector: 'app-close',
  templateUrl: './close.component.html',
  styleUrls: ['./close.component.scss']
})
export class CloseComponent implements OnInit, OnDestroy {
  @Input() data: any;
  message: any;
  form: FormGroup = this.formBuilder.group({
    final_service_fee: ['0'],
    final_service_fee_reasons: ['', Validators.required]
  });
  broadcast$: Subscription;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly broadcast: BroadcastService,
    public activeModal: NgbActiveModal,
    private readonly tickets: TicketsService,
    private readonly swal: SwalService,
    private readonly fromService: FromService
  ) { }

  ngOnInit(): void {

    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'tickets-technitians-row-click': this.message = event.data; break;
      }
    }
    );
    this.fromService.setForm(this.form);
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  closeTicket(): void {
      this.swal.warning(
        {title: '¿Estás seguro de cerrar el ticket y efectuar el cobro al cliente?'}
        ).then(result => {
          if (result.value) {
            const params = { 
              status                    : 'closed', 
              final_service_fee         : this.form.controls.final_service_fee.value,
              final_service_fee_reasons : this.form.controls.final_service_fee_reasons.value};
            this.tickets.getStatus(params, this.data.id_tickets).subscribe((data: any) => {
              if (data.success) {
                this.swal.success().then(() => {
                  this.activeModal.dismiss();
                });
              } else {
                this.swal.error({title: 'Ocurrió un error al actualizar los datos'});
              }
          });
          }
      });
  }

}
