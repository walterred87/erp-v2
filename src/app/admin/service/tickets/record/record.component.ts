import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { TicketsService } from '../../../../services/tickets.service';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss']
})
export class RecordComponent implements OnInit, OnDestroy {
  @Input() data: any;
  historyConfig = {
    config: {
      type: 'tickets',
      base: this.tickets,
      api: 'getHistory',
      params: { history: 0 },
      order: [[0, 'desc']]
    },
    rows: [
      {
        display: 'Estatus',
        field: 'id'
      }
    ]
  };
  history = [];
  hist$: Subscription;
  constructor(
    private readonly tickets: TicketsService,
    public activeModal: NgbActiveModal,
    private readonly _historyService: TicketsService) { }

  ngOnInit(): void {
    this.hist$ = this._historyService.getHistory(this.data.id_tickets).subscribe((data: any) => {
      data.forEach((element, index) => {
        let type = '';
        switch (index) {
          case 0: type = 'assignations'; break;
          case 1: type = 'completions'; break;
          case 2: type = 'confirmations'; break;
        }
        element.response.forEach(itm => {
          this.history.push({ type, ...itm});
        });
        this.sortData();
      });

    });
  }

  sortData(): Array<any> {
    return this.history.sort((a, b) =>
      (new Date(b.created_at) as any) - (new Date(a.created_at) as any));
  }
  
  ngOnDestroy(): void {
    this.hist$.unsubscribe();
  }

}
