import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SellersRoutes } from './sellers.routing';
import { SalesGroupComponent } from './sales-group/sales-group.component';
import { ComponentsModule } from '../../components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FromService } from '../../providers/form.service';
import { ExtrasComponent } from './extras/extras.component';
import { CommissionsComponent } from './commissions/commissions.component';
import { ClientInvalidComponent } from './commissions/client-invalid/client-invalid.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SellersRoutes),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [
    SalesGroupComponent,
    ExtrasComponent,
    CommissionsComponent
  ],
  exports: [
    SalesGroupComponent
  ],
  entryComponents: [],
  providers: [
    FromService
  ]
})
export class SellersModule { }
