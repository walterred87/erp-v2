import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, filter, take } from 'rxjs/operators';
import { AuthorizationService } from '../services/authorization.service';
import { Router } from '@angular/router';
import { SwalService } from '../services/swal.service';

@Injectable({
  providedIn: 'root'
})

// tslint:disable: no-parameter-reassignment
// tslint:disable: unnecessary-else
// tslint:disable: prefer-function-over-method
export class AuthorizationInterceptorService implements HttpInterceptor {

  private readonly invalidErrorMessages = [
    'token_invalid',
    'token_error',
    'user_not_found'
  ];
  private isRefreshing = false;
  private readonly refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  
  constructor(
    private readonly router: Router,
    private readonly swal: SwalService,
    public authService: AuthorizationService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this.authService.token) {
      request = this.addToken(request, this.authService.token);
    }

    return next.handle(request).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 401 || this.invalidErrorMessages.indexOf(error.error) > -1) {
            return this.handle401Error(request, next);
          }
        } else {
          return throwError(error);
        }
      })
    );
  }

  // tslint:disable-next-line: prefer-function-over-method
  private addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      },
      withCredentials: true
    });
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);

      return this.authService.refreshToken().pipe(
        switchMap((data: any) => {
          this.isRefreshing = false;
          this.refreshTokenSubject.next(data.token);

          return next.handle(this.addToken(request, data.token));
        })
      );
    } else {
      this.refreshTokenSubject.pipe(
        filter(token => token !== null),
        take(1),
        switchMap((token: any) => next.handle(this.addToken(request, token)))
      );
    }
  }
}
