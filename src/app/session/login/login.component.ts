import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../services/authorization.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [AuthorizationService]
})
export class LoginComponent {
  loginForm: FormGroup = this.formBuilder.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    appVersion: ['']
  });
  
  errors = [];
  loading = false;

  constructor(
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly authorizationService: AuthorizationService,
    private readonly userService: UsersService
  ) {
    const access_token = localStorage.getItem('access_token');
    const user = localStorage.getItem('user');

    if (access_token != null && user != null) {
      this.loading = true;
      const userData = JSON.parse(user);
      this.userService.user(userData.id).subscribe((data: any) => {
        if (data.success) {
          this.router.navigate(['admin/inicio']);
        } else {
          localStorage.removeItem('access_token');
          localStorage.removeItem('user');
        }
      });
    }

  }

  logIn(): void {
    this.errors = [];

    if (this.loginForm.valid) {
      this.loading = true;
      const formData = this.loginForm.value;

      this.authorizationService.login(this.loginForm.value).subscribe((data: any) => {
        localStorage.setItem('access_token', data.access_token);
        localStorage.setItem('user', JSON.stringify(data.user));
        // this.toastr.success('!Has ingresado exitosamente!');
        this.router.navigate(['/admin/inicio']);
      });
    }
  }

}
