import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetFieldPipe } from './get-field.pipe';

const pipes = [
  GetFieldPipe
];
@NgModule({
  declarations: [...pipes],
  imports: [
    CommonModule
  ],
  exports: [...pipes]
})
export class PipesModule { }
