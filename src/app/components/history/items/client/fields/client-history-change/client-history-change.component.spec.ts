import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientHistoryChangeComponent } from './client-history-change.component';

describe('ClientHistoryChangeComponent', () => {
  let component: ClientHistoryChangeComponent;
  let fixture: ComponentFixture<ClientHistoryChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientHistoryChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientHistoryChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
