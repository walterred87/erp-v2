import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-history-change',
  templateUrl: './client-history-change.component.html',
  styleUrls: ['./client-history-change.component.scss']
})
export class ClientHistoryChangeComponent implements OnInit {
  @Input('item') item: any;
  
  ngOnInit(): void {
    //
  }

}
