import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadStatusComponent } from './bad-status.component';

describe('BadStatusComponent', () => {
  let component: BadStatusComponent;
  let fixture: ComponentFixture<BadStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
