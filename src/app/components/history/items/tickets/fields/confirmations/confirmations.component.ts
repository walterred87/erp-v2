import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-confirmations',
  templateUrl: './confirmations.component.html',
  styleUrls: ['./confirmations.component.scss']
})
export class ConfirmationsComponent implements OnInit {
  @Input('item') item: any;

  // tslint:disable-next-line: typedef
  ngOnInit() {
    // tslint:disable-next-line: no-empty
  }
}
