import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, forkJoin } from 'rxjs';
@Injectable({
  providedIn: 'root'
})

export class ClientsService {
  baseUrl = `${environment.apiUrl}`;

  constructor(private readonly http: HttpClient) { }

  getClients(params?, filters?): Observable<any> {
    if (filters) {
      Object.assign(params, filters);
      // params['status'] = filters['status'];
      // params['serial_number'] = filters['serial_number'];
      // params['sales_agent'] = filters['sales_agent'];
    }

    return this.http.post(`${this.baseUrl}/clients/get_clients`, params);
  }

  show(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/clients/${id}`);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${this.baseUrl}/clients/${id}`);
  }

  setStatus(id?, params?): Observable<any> {
    return this.http.post(`${this.baseUrl}/clients/${id}/setStatus`, params);
  }

  subscribeToPlan(id, params?): Observable<any> {
    return this.http.post(`${this.baseUrl}/clients/${id}/subscribeToPlan`, params);
  }

  createOfflineCharge(params): Observable<any> {
    return this.http.post(`${this.baseUrl}/charges`, params);
  }

  subscribeToPlanOffline(id, params): Observable<any> {
    return this.http.post(`${this.baseUrl}/clients/${id}/subscribeToPlanOffline`, params);
  }

  makeSalesAgent(id?, params?): Observable<any> {
    return this.http.post(`${this.baseUrl}/clients/${id}/attachRole`, params);
  }

  update(id, params?): Observable<any> {
    return this.http.put(`${this.baseUrl}/clients/${id}`, params);
  }

  getHierarchicalTree(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/clients/${id}/hierarchicalTree`);
  }

  getCards(params, filters?): Observable<any> {
    let id = 0;
    if (filters) {
      id = filters.id_clients;
    }

    return this.http.post(`${this.baseUrl}/client/${id}/getCards`, params);
  }

  getImages(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/clients/${id}/getImages`);
  }

  getHistory(id): any {
    const id_clients = id.id_clients;
    const id_users = id.id_users;
    const paramsClient = {
      params: { id_clients }
    };
    const paramsUser = {
      params: { id_users }
    };

    const corrections   = this.http.get(`${this.baseUrl}/corrections`, paramsClient);
    const invalidations = this.http.get(`${this.baseUrl}/invalidations`, paramsClient);
    const rejections    = this.http.get(`${this.baseUrl}/rejections`, paramsClient);
    const cancellations = this.http.get(`${this.baseUrl}/cancellations`, paramsClient);
    const notifications = this.http.get(`${this.baseUrl}/notifications`, paramsUser);

    return forkJoin([corrections, invalidations, rejections, cancellations, notifications]);
  }

  getLogs(params): Observable<any> {
    const logParams = {
      params: {
        id_users: params.id_users,
        id_clients: params.id_clients
      }
    };
    
    return this.http.get(`${this.baseUrl}/logs`, logParams);
  }
}
