import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ErrrorCodesService {

  baseUrl = `${environment.apiUrl}`;

  constructor(private readonly http: HttpClient) { }

  create(params): any {
    return this.http.post(`${this.baseUrl}/error-codes`, params);
  }

  delete(id): any {
    return this.http.delete(`${this.baseUrl}/error-codes/${id}`);
  }

  edit(id, params): any {
    return this.http.put(`${this.baseUrl}/error-codes/${id}`, params);
  }

  getErrorCodes(params?): any {
    return this.http.post(`${this.baseUrl}/error-codes/getErrorCodes`, params);
  }

  show(id): any {
    return this.http.get(`${this.baseUrl}/error-codes/${id}`);
  }

  index(): any {
    return this.http.get(`${this.baseUrl}/error-codes`);
  }
}
