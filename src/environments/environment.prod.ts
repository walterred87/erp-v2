export const environment = {
  production: true,
  apiUrl: 'https://www.panelaguagente.xyz:8073/demo/v3/api/public',
  months_ahead: [
    {
      id: 1,
      name: 1
    }, 
    {
      id: 3,
      name: 3
    }, 
    {
      id: 6,
      name: 6
    }, 
    {
      id: 12,
      name: 12
    }
  ],
  payment_types: [
    {
      name: 'OXXO',
      id: 'OXXO'
    }, 
    {
      name: 'SPEI',
      id: 'SPEI'
    }
  ],
  error_codes_categories: [
    {
      id: 'Basic Codes',
      name: 'Basic Codes'
    },
    {
      id: 'Detailed Codes',
      name: 'Detailed Codes'
    },
    {
      id: 'Other',
      name: 'Other'
    }
  ]
};
