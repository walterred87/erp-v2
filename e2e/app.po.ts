import { browser, element, by } from 'protractor';

export class CoreUIPage {
  navigateTo(): any {
    return browser.get('/');
  }

  getParagraphText(): any {
    return element(by.className('app-footer')).getText();
  }
}
